from selenium import webdriver
from selenium.webdriver.common.keys import Keys
via_path = 'https://www.robots.ox.ac.uk/~vgg/software/via/app/via_subtitle_annotator.html'
driver_path = '/home/nathanasiou/drivers/geckodriver-v0.31.0-linux64'

# browser = webdriver.Firefox(executable_path=driver_path)
# browser.get(via_path)

from selenium import webdriver
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
# options = Options()
# options.add_argument('-headless')
# driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()), options=options)
# driver.get(via_path)

def test_firefox_session():
    service = FirefoxService(executable_path=GeckoDriverManager().install())
    options = Options()
    options.headless = True
    driver = webdriver.Firefox(service=service, options=options)
    driver.get(via_path)
    button_add_vid = driver.find_element(By.ID, "add_media_local")
    button_add_vid.send_keys('~/shared_logs/fast_renders/1085_allground.mp4')
    driver.quit()

test_firefox_session()
# browser.find_element_by_id('add_media_local').send_keys()
