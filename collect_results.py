from pathlib import Path
from teach.utils.file_io import write_json
import os

base_path = Path('/is/cluster/work/nathanasiou/experiments/teach-post-submission-29Aug')
experiments = ['hist-10-bs-32', 'hist-15-bs-32-lmd_kl-1e-4', 'hist-15-bs-32-lr-1e-5', 'hist-15-bs-32-z_prev-w-hist', 'hist-15-bs-32', 
'hist-15-bs-32-lmd_kl-1e-6', 'hist-15-bs-32-lr-5e-4', 'hist-15-bs-32-z_prev-only', 'hist-5-bs-32']
experiments = ['teach-hf-1-motion_branch_true-teacher_force_false',
               'teach-hf-2-motion_branch_true-teacher_force_false',
               'teach-hf-5-motion_branch_true-teacher_force_false',
               'teach-hf-10-motion_branch_true-teacher_force_false']

sample_variants = ['samples_slerp_aligned_pairs']
ckpt_path = 'checkpoint-last/'
dir = Path('/home/nathanasiou/Desktop/scratch/teach/quant')
dir.mkdir(exist_ok=True, parents=True)
metr_d = {}
for expname in experiments:
    for sample_variant in sample_variants:
        path = Path( base_path / expname / sample_variant / ckpt_path)
        file_path = path / 'babel_metrics_pairs_val'
        if not file_path.is_file():
            print(file_path)
            continue
        with open(file_path, 'r') as f:
            lines = f.readlines()
            lines = lines[:8]
            lines = [l.strip() for l in lines]
            metric_name, metric_val = zip(*(s.split(":") for s in lines))
            try:
                metric_val = [round(float(x.strip().split("'")[1]), 6) for x in metric_val] 
                metric_val = [float(str(x).ljust(5, '0')) for x in metric_val]

            except:
                print('buggy file at:', file_path)
                continue        
            name = f'{expname}_{sample_variant}'
            metr_d[name] = dict(zip(metric_name, metric_val))

write_json(metr_d, f'{dir}/final_mets.json')
list_of_tup_expname_metrs = [(k.replace('teacher_force', 'TF').replace('motion_branch', 'MB'),v) for k,v in metr_d.items()]
exp_names, only_scores = zip(*list_of_tup_expname_metrs)
metr2scorelist_d = {k: [d[k] for d in only_scores] for k in only_scores[0]}
import pandas as pd
df = pd.DataFrame(metr2scorelist_d)
df.index = exp_names
# pretty array
header1 = [x.split('_')[0] for x in list(df.columns)]
header2 = ['_'.join(x.split('_')[1:]) for x in list(df.columns)]
df.columns = [header1, header2]
df.sort_index()
html = df.to_html(classes='table table-striped text-center', justify='center')
  
# write html/tex to file
with open(f'{dir}/metrics.html', "w") as f:
    f.write(html)
with open(f'./metrics.html', "w") as f:
    f.write(html)

latex = df.to_latex(index=True)
with open(f'./metrics.tex', "w") as f:
    f.write(latex)

with open(f'{dir}/metrics.tex', "w") as f:
    f.write(latex)
