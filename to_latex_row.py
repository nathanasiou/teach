import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--f', required=True, type=str, help='file to metrics')
    args = parser.parse_args()
    f = args.f
    with open(f, 'r') as fid:
        m_raw = fid.readlines()
    m = [x.strip().split(':')[1] for x in m_raw[:8]]
    
    m = [round(float(x.strip().split("'")[1]), 3) for x in m] 
    m = [str(x).ljust(5, '0') for x in m]
    ltx =' & '.join(m)
    print('\n')
    print('& ' + ltx)
# x['hyper_parameters']['motionencoder']['_target_'] = x['hyper_parameters']['motionencoder']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['textencoder']['_target_'] = x['hyper_parameters']['textencoder']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['transforms']['rots2rfeats']['_target_'] = x['hyper_parameters']['transforms']['rots2rfeats']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['transforms']['rots2joints']['_target_'] = x['hyper_parameters']['transforms']['rots2joints']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['transforms']['joints2jfeats']['_target_'] = x['hyper_parameters']['transforms']['joints2jfeats']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['transforms']['_target_'] = x['hyper_parameters']['transforms']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['_target_'] = x['hyper_parameters']['losses']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_text2rfeats_vel_0_func']['_target_'] = x['hyper_parameters']['losses']['recons_text2rfeats_vel_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_text2rfeats_vel_1_func']['_target_'] = x['hyper_parameters']['losses']['recons_text2rfeats_vel_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_text2rfeats_1_func']['_target_'] = x['hyper_parameters']['losses']['recons_text2rfeats_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_text2rfeats_0_func']['_target_'] = x['hyper_parameters']['losses']['recons_text2rfeats_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_rfeats2rfeats_0_func']['_target_'] = x['hyper_parameters']['losses']['recons_rfeats2rfeats_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_rfeats2rfeats_1_func']['_target_'] = x['hyper_parameters']['losses']['recons_rfeats2rfeats_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_jfeats2jfeats_1_func']['_target_'] = x['hyper_parameters']['losses']['recons_jfeats2jfeats_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['recons_jfeats2jfeats_0_func']['_target_'] = x['hyper_parameters']['losses']['recons_jfeats2jfeats_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['latent_manifold_0_func']['_target_'] = x['hyper_parameters']['losses']['latent_manifold_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['latent_manifold_1_func']['_target_'] = x['hyper_parameters']['losses']['latent_manifold_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_text_0_func']['_target_'] = x['hyper_parameters']['losses']['kl_text_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_text_1_func']['_target_'] = x['hyper_parameters']['losses']['kl_text_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_motion_1_func']['_target_'] = x['hyper_parameters']['losses']['kl_motion_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_motion_0_func']['_target_'] = x['hyper_parameters']['losses']['kl_motion_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_text2motion_0_func']['_target_'] = x['hyper_parameters']['losses']['kl_text2motion_0_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_text2motion_1_func']['_target_'] = x['hyper_parameters']['losses']['kl_text2motion_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_motion2text_1_func']['_target_'] = x['hyper_parameters']['losses']['kl_motion2text_1_func']['_target_'].replace('temos.', 'teach.')
# x['hyper_parameters']['losses']['kl_motion2text_0_func']['_target_'] = x['hyper_parameters']['losses']['kl_motion2text_0_func']['_target_'].replace('temos.', 'teach.')
