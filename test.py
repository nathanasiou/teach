import logging
from typing import Optional

import hydra
from hydra import compose
from hydra.core.hydra_config import HydraConfig
from hydra.utils import to_absolute_path
from omegaconf import DictConfig, OmegaConf

import os
from os.path import join

logger = logging.getLogger(__name__)
os.environ["HYDRA_FULL_ERROR"] = "1"


@hydra.main(config_path="configs", config_name="load")
def _test(cfg: DictConfig) -> None:
    return test(cfg)


# entry point for jupyter notebook
def test(cfg: DictConfig, hydra_conf: Optional[DictConfig] = None) -> None:
    if cfg.path is None:
        raise TypeError("You should add the load_checkpoint file.")

    output_dir = to_absolute_path(cfg.path)
    original_overrides = OmegaConf.load(join(output_dir, ".hydra/overrides.yaml"))

    if hydra_conf is None:
        hydra_conf = HydraConfig.get()

    current_overrides = hydra_conf.overrides.task

    hydra_config = OmegaConf.load(join(output_dir, ".hydra/hydra.yaml"))
    # getting the config name from the previous job.
    config_name = hydra_config.hydra.job.config_name
    # concatenating the original overrides with the current overrides

    original_overrides_dict = {key: val for name in original_overrides for key, val in [name.split("=")]}
    current_overrides_dict = {key: val for name in current_overrides for key, val in [name.split("=")]}

    if 'machine.trainer.gpus' in original_overrides_dict:
        del original_overrides_dict['machine.trainer.gpus']
    del current_overrides_dict['path']

    overrides = [f"{key}={val}" for key, val in {**original_overrides_dict, **current_overrides_dict}.items()]

    # compose a new config from scratch
    cfg = compose(config_name, overrides=overrides)

    import pytorch_lightning as pl
    import torch
    from hydra.utils import instantiate
    pl.seed_everything(1234)

    # Instantiate data module
    data_module = instantiate(cfg.data)

    # Instantiate the model
    model = instantiate(cfg.model,
                        njoints=data_module.njoints,
                        nfeats=data_module.nfeats,
                        _recursive_=False)

    # Choose the device: TODO, find a better way
    device = "cuda" if torch.cuda.is_available() else "cpu"

    # Load the last checkpoint
    best_ckpt_path = join(output_dir, "checkpoints/last.ckpt")
    model.load_state_dict(torch.load(best_ckpt_path, map_location=device)["state_dict"])

    # fast_dev_run
    trainer = pl.Trainer(
        **OmegaConf.to_container(cfg.trainer, resolve=True),
        # resume_from_checkpoint=best_ckpt_path,
        logger=None,
    )

    dataloader = data_module.debug_dataloader
    predicted_train = trainer.predict(model, dataloader())
    pred_posedata = predicted_train[0].to("cpu")

    batches_train = [x for x in dataloader()]
    batch = batches_train[0]
    batch["pred_pose_data"] = pred_posedata

    return batch
    pred_posedata = batch["pose_data"]
    input_data_ = batch["pose_data"].to("cpu")

    poses_diff = predicted_poses - input_poses
    trans_diff = predicted_trans - input_trans

    batch_l2m_poses_loss = torch.nn.functional.smooth_l1_loss(predicted_poses, input_poses).item()
    batch_l2m_trans_loss = torch.nn.functional.smooth_l1_loss(predicted_trans, input_trans).item()
    print(f"{batch_l2m_poses_loss=}")
    print(f"{batch_l2m_trans_loss=}")

    a = 1
    # train
    print("Running in ", os.getcwd())
    print(OmegaConf.to_yaml(cfg))
    import ipdb
    ipdb.set_trace()


if __name__ == '__main__':
    _test()
