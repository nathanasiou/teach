import os
import sys
import logging
import hydra
from pathlib import Path
from omegaconf import DictConfig
from omegaconf import OmegaConf
import numpy as np
import glob

from teach.render.mesh_viz import visualize_meshes
from teach.render import render_animation

from teach.render.anim import render_animation, render_animation_parallel
from teach.render.video import save_video_samples
import teach.launch.prepare  # noqa
from tqdm import tqdm
from teach.utils.file_io import read_json
from teach.launch.prepare import get_last_checkpoint
import torch

logger = logging.getLogger(__name__)

plt_logger = logging.getLogger("matplotlib.animation")
plt_logger.setLevel(logging.WARNING)
os.environ['PYOPENGL_PLATFORM'] = 'egl'

@hydra.main(config_path="configs", config_name="render_video_fast")
def _render(cfg: DictConfig):
    return render(cfg)

def chunker(it,size):
    rv = [] 
    for i,el in enumerate(it,1) :   
        rv.append(el)
        if i % size == 0 : 
            yield rv
            rv = []
    if rv : yield rv

def render(cfg: DictConfig) -> None:
    from pathlib import Path
    if cfg.mode == 'npy':

        filepath = Path(cfg.files)
        import glob
        if (cfg.files).endswith('.npy'):
            paths = [cfg.files]
        else:
            paths = glob.glob(f'{cfg.files}/*')
        
        logger.info("Rendering video")
        import numpy as np

        for path in tqdm(paths):
            try:
                gen_sample = np.load(path, allow_pickle=True).item()
                motion = gen_sample['motion']
                text = gen_sample['text']
            except FileNotFoundError:
                raise FileNotFoundError
            posix_p = Path(path)
            vid_path = posix_p.parent / posix_p.stem
            if motion.shape[1] < 100:
                from teach.info.joints import mmm_to_smplh_scaling_factor
                motion /= mmm_to_smplh_scaling_factor
                render_animation(motion, output=f'{vid_path.resolve()}.mp4', title=text,
                                fps=30)
            else:
                vid_ = visualize_meshes(motion)
                save_video_samples(vid_, vid_path.resolve(), text, fps=30)

    else:
        from teach.data.tools.collate import collate_pairs_and_text

        cfg_p = Path(cfg.folder)

        output_dir = Path(hydra.utils.to_absolute_path(cfg_p))
        print(hydra.utils.to_absolute_path(cfg_p), cfg_p)
        last_ckpt_path = get_last_checkpoint(cfg_p, ckpt_name='299')
        # use this path for oracle experiment on kit-xyz
        # '/is/cluster/work/nathanasiou/experiments/temos-original/last.ckpt'
        # Load previous config
        prevcfg = OmegaConf.load(output_dir / ".hydra/config.yaml")
        # use this path for oracle experiment on kit-xyz
        # OmegaConf.load('/is/cluster/work/nathanasiou/experiments/temos-original/config.yaml')

        # Overload it
        cfg = OmegaConf.merge(prevcfg, cfg)
        if cfg.slerp:
            slerp_window = 8
        else:
            slerp_window = None        
        storage = output_dir / f'videos/'
        storage.mkdir(exist_ok=True,  parents=True)

        import pytorch_lightning as pl
        import numpy as np
        from hydra.utils import instantiate
        pl.seed_everything(cfg.seed)
        logger.info("Loading data module")

        # only pair evaluation to be fair
        naive_bsl = False
        if cfg.data.dtype in ['pairs', 'pairs_only']:
            naive_bsl = True
            cfg.data.dtype = 'separate_pairs'
        # Instantiate all modules specified in the configs
        model = instantiate(cfg.model,
                            nfeats=135,
                            logger_name="none",
                            nvids_to_save=None,
                            _recursive_=False)

        logger.info(f"Model '{cfg.model.modelname}' loaded")

        # Load the last checkpoint
        model = model.load_from_checkpoint(last_ckpt_path)
        model.eval()
        # align_full_bodies = True if cfg.align == 'full' else False
        # align_trans = True if cfg.align == 'trans' else False

        logger.info("Model weights restored")
        labels_dict = read_json('./deps/inference/labels.json')
    
        if not cfg.walk:
            ## load also groundtruth
            logger.info("Loading data module")
            cfg.data.dtype = 'separate_pairs'
            data_module = instantiate(cfg.data)
            logger.info(f"Data module '{cfg.data.dataname}' loaded")
            dataset_train = getattr(data_module, f"train_dataset")
            dataset_val = getattr(data_module, f"val_dataset")

        model.transforms.rots2joints.jointstype = cfg.jointstype
        ##
        from teach.utils.inference import train_pairs_render, val_pairs_render, walk_train, walk_val

        with torch.no_grad():
            if cfg.walk:
                for set_name, labels in zip(['train', 'val'], [walk_train, walk_val]):
                    cur_p = storage / f'walk_{set_name}'
                    if cfg.jointstype == 'vertices':
                        cur_p = cur_p / 'bodies'
                    else:
                        cur_p = cur_p / 'skeletons'
                    cur_p.mkdir(exist_ok=True,  parents=True)

                    for text in (pbar := tqdm(labels)):
                        vids_dur = []
                        for i in range(1,8):
                            cur_lens = [30*i]
                            cur_texts = [text]

                            # if cfg.model.modelname == 'temos' and naive_bsl:
                            #     cur_lens = [sum(cur_lens)]
                            #     cur_texts = [', '.join(cur_texts)]
                            #     assert len(cur_lens) == len(cur_texts) == 1

                            # if cur_lens[0] < 15:
                            #     cur_lens[0] += 15 - cur_lens[0]

                            # fix the seed
                            from teach.transforms.smpl import RotTransDatastruct
                            motion = model.forward_seq(cur_texts, cur_lens,
                                                    align_full_bodies=True,
                                                    align_only_trans=False,
                                                    slerp_window_size=slerp_window,
                                                    return_type=cfg.jointstype)
                            from teach.render.video import stack_vids
                            
                            if motion.shape[1] < 100:
                                motion = motion.numpy()
                                from teach.info.joints import mmm_to_smplh_scaling_factor

                                motion /= mmm_to_smplh_scaling_factor
                                render_animation(motion,
                                                output=f'{cur_p.resolve()}/{text}_{30*i}.mp4',
                                                title=text,
                                                fps=30)
                            else:
                                vid_ = visualize_meshes(motion)
                                vid_gen_p = save_video_samples(vid_, f'{cur_p.resolve()}/{text}_{30*i}.mp4',
                                                                text, fps=30)
            else:
                
                for set_name, keyids in zip(['train', 'val'], [train_pairs_render, val_pairs_render]):
                    cur_p = storage / f'{set_name}'
                    if cfg.jointstype == 'vertices':
                        cur_p = cur_p / 'bodies'
                    else:
                        cur_p = cur_p / 'skeletons'
                    cur_p.mkdir(exist_ok=True,  parents=True)

                    for keyid in (pbar := tqdm(keyids)):
                        ###
                        dataset_split = dataset_train if set_name == 'train' else dataset_val
                        try:
                            groundtruth_motion = dataset_split.load_keyid(keyid, mode='inference')['datastruct']
                        except:
                            logger.info(f'Keyid {keyid} not loaded properly. Skipping example.')
                            continue
                        ds = model.transforms.Datastruct(features=groundtruth_motion) 
                        gt_ds = ds.rots
                        gt_rots, gt_trans = gt_ds.rots, gt_ds.trans
                        from teach.transforms.smpl import RotTransDatastruct

                        final_datastruct = model.transforms.Datastruct(rots_=RotTransDatastruct(rots=gt_rots, trans=gt_trans))

                        gt_verts = final_datastruct.vertices
                        ####
                        if set_name == 'train':
                            one_data = labels_dict['train'][keyid]
                        elif set_name == 'val':
                            one_data = labels_dict['val'][keyid]
                        cur_lens = [one_data['len'][0], one_data['len'][1] + one_data['len'][2]]
                        cur_texts = [one_data['text'][0], one_data['text'][1]]

                        if cfg.model.modelname == 'temos' and naive_bsl:
                            cur_lens = [sum(cur_lens)]
                            cur_texts = [', '.join(cur_texts)]
                            assert len(cur_lens) == len(cur_texts) == 1

                        if cur_lens[0] < 15:
                            cur_lens[0] += 15 - cur_lens[0]

                        # fix the seed
                        from teach.transforms.smpl import RotTransDatastruct
                        motion = model.forward_seq(cur_texts, cur_lens,
                                                align_full_bodies=True,
                                                align_only_trans=False,
                                                slerp_window_size=slerp_window,
                                                return_type=cfg.jointstype)
                        from teach.render.video import stack_vids
                        
                        if motion.shape[1] < 100:
                            motion = motion.numpy()
                            from teach.info.joints import mmm_to_smplh_scaling_factor

                            motion /= mmm_to_smplh_scaling_factor
                            render_animation(motion,
                                            output=f'{cur_p.resolve()}/{keyid}.mp4',
                                            title=','.join(cur_texts),
                                            fps=30)
                        else:
                            gt_vid = visualize_meshes(gt_verts)
                            vid_gt_p = save_video_samples(gt_vid, f'{cur_p.resolve()}/{keyid}_gt.mp4',
                                                ','.join(cur_texts), fps=30)

                            vid_ = visualize_meshes(motion)
                            vid_gen_p = save_video_samples(vid_, f'{cur_p.resolve()}/{keyid}',
                                            ','.join(cur_texts), fps=30)
                            stack_vids([vid_gen_p, vid_gt_p], f'{cur_p.resolve()}/{keyid}_vs_gt.mp4')

if __name__ == '__main__':
    _render()
