from dataclasses import asdict
import subprocess
from pathlib import Path
import sys
from loguru import logger
import glob


v=True 
def end_script():
    from inspect import currentframe, getframeinfo
    frameinfo = getframeinfo(currentframe())
    sys.exit(f'Exited after line {frameinfo.filename} {frameinfo.lineno}')
    
def run(cmd):
    logger.info(f"Executing: {' '.join(cmd)}")
    x = subprocess.call(cmd)

# teach/independent sampling
voutdir = "./for_site/videos"
ioutdir = "./for_site/images"
outdir =  "./for_site/"

prompts = [('[stand with bent knees, kick with right leg, kick with left foot]', '[0.7, 1.2, 2.3]'),
           ('[jump with left foot, walk backwards, crawl]', '[1.5, 1.5, 2]'),
           ('[jump forwards, walk with knees bent, sit down]', '[2.1, 2.8, 2]'),
           ('[jump with left foot, walk forwards, sit down]', '[1.5, 2.5, 2]'),
           ('[walk like a drunk, stumble over something, fall down]', '[1.9, 1.2, 1.4]'),
           ('[hop forward, squat down, crouch, lay on a bench]', '[2, 1.5, 1.9, 2.5]'),
           ('[pick an object from the ground, carry object, lift object with right hand, put object on table]', '[1.5, 1.1, 1.2, 1]'),
           ('[pick an object from the ground, lift object with right hand, put object on the ground]', '[1.5, 1.1, 1.2]'),
           ('[balance on two feet, perform yoga pose, stretch arms, step forward with left foot]', '[2.8, 1.8, 1.8, 1.2]'),
           ('[scratch head with right hand, limp forward, turn around, knock, open the door]', '[0.8, 2.5, 1.1, 1, 1.5]'),
           ('[walk backwards, grab item with left hand, prepare drink, drink]', '[2.1, 1.1, 3.1, 1.5]'),
           ('[right side lunges, left side lunges]', '[1.5, 1.9]'),
           ('[hold golf club while look at the ground, swing golf club, jog forward, kneel down]', '[0.8, 1.4, 1.2, 1.3]'),
           ('[punches with right hand,punches with left hand, kung fu pose, sidekick motion with right foot]', '[1, 1, 1.2, 1.2]'),
           ('[rolls left ankle in front of body, swings full left leg forwards and backwards in front of body]', '[2.8, 3.2]'),
           ('[rotate neck, swing left leg in and out]', '[1.4, 4.9]'),
           ('[throw ball, catch ball with left hand, toss ball with left hand, drop ball in front of right foot]', '[1.2, 1.3, 1.2,0.8]'),
           ('[grab ball with both hands, bounce basketball, shoots basketball]', '[0.7,1.6,1.2]'),
           ('[step back, shoot basketball, run forwards]', '[2, 2, 2.1]'),
           ('[walk on toes, turn right,lie down on floor]', '[3,1,2.1]'),
           ('[step on the left, sit down, play the piano]', '[1.1, 1.2, 1.1]'),
           ('[play an instrument, nod head, wave right arm]', '[1, 0.3, 1]'),
           ('[stretch to side bend over, stretch head, play violin]', '[1.9, 1.6, 2.8]'),
           ('[wave left hand, raise right hand, step to the right, walk in a circle, sit down, stand up, hop over obstacle]', '[1.1, 1.2, 1.5, 2.5, 1.8,1, 1.9]'),
           ('[jump backwards, jump forwards, roll right arm, spin arms in circles]', '[1, 1.2, 1.6, 1.7]'),
           ('[lift object up, spin arms, jump spin]', '[1.7, 1.6, 1.2]'),
           ('[walk up stairs, walk down stairs backwards, climb up]', '[1.2, 1.1, 1.3]')]

exp_path='/is/cluster/work/nathanasiou/experiments/teach-post-submission-29Aug/teach-hf-5-motion_branch_true-teacher_force_false/'

nos = 3
for i, (text, dur) in enumerate(prompts):
    for j in range(nos):
        cmd = ['python', 'interact_teach.py', f'folder={exp_path}', f'output={outdir}/example-{i}', 'samples=3', f'texts={text}', f'durs={dur}']
        run(cmd)
