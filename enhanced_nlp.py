import glob
import json
from tqdm import tqdm
from nlp_actions.textenhanced import get_newsent


def process_nlp():
    nlp_paths = glob.glob("datasets/kit/*_annotations.json")

    for nlp_path in tqdm(nlp_paths, desc="processing"):
        with open(nlp_path) as nlp_file:
            data = nlp_file.read()

        # list of sentences
        sentences = json.loads(data)
        if not sentences:
            continue

        # remove advertising annotations
        sentences = [t for t in sentences if "http" not in t]

        if not sentences:
            continue

        all_newsent = []
        for sent in sentences:
            newsent = get_newsent(sent)
            all_newsent.append(newsent)

        # print(all_newsent)
        action_path = nlp_path.replace("_annotations.json", "_annotations_canonicalized.json")

        with open(action_path, "w") as action_file:
            json.dump(all_newsent, action_file)

    from collections import defaultdict
    hist_verbs = defaultdict(int)
    for verb in all_verbs:
        hist_verbs[verb] += 1

    # saving the verbs for stats
    with open("verbs.json", "w") as verb_file:
        json.dump(hist_verbs, verb_file)


if __name__ == '__main__':
    process_nlp()
