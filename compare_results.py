import argparse
import os
import logging
from omegaconf import DictConfig, OmegaConf
import hydra
import teach.launch.prepare  # noqa
from tqdm import tqdm
from hydra.utils import instantiate
from pathlib import Path
import pytorch_lightning as pl
import torch 
from teach.render.video import save_video_samples
from teach.render.mesh_viz import visualize_meshes
from teach.render.video import stack_vids_moviepy, add_text_moviepy
import moviepy as mp
logger = logging.getLogger(__name__)

base_dir = '/is/cluster/work/nathanasiou/experiments/teach/babel-amass'


@hydra.main(config_path="configs", config_name="compare_results")
def _compare_results(cfg: DictConfig):
    return compare_results(cfg)

def compare_results(newcfg: DictConfig) -> None:
    cfg.data.dtype = 'separate_pairs'
    data_module = instantiate(cfg.data)
    logger.info(f"Data module '{cfg.data.dataname}' loaded")
    val_dataset = data_module.val_dataset
    train_dataset = data_module.train_dataset
    # for some keys and durations and labels:
        # Load last config

    output_dir = Path(hydra.utils.to_absolute_path(newcfg.folder))
    last_ckpt_path = newcfg.last_ckpt_path
    # use this path for oracle experiment on kit-xyz
    # '/is/cluster/work/nathanasiou/experiments/temos-original/last.ckpt'
    # Load previous config
    prevcfg = OmegaConf.load(output_dir / ".hydra/config.yaml")
    # use this path for oracle experiment on kit-xyz
    # OmegaConf.load('/is/cluster/work/nathanasiou/experiments/temos-original/config.yaml')

    # Overload it
    cfg = OmegaConf.merge(prevcfg, newcfg)
    savepath = cfg.output_dir
    if cfg.evaluate_pairs:
        slerp_window = 10

    savepath.mkdir(exist_ok=True, parents=True)
    model = instantiate(cfg.model,
                        nfeats=data_module.nfeats,
                        logger_name="none",
                        nvids_to_save=None,
                        _recursive_=False)

    logger.info(f"Model '{cfg.model.modelname}' loaded")

    # Load the last checkpoint
    model = model.load_from_checkpoint(last_ckpt_path)
    logger.info("Model weights restored")
    model.sample_mean = cfg.mean
    model.fact = cfg.fact

    if not model.hparams.vae and cfg.number_of_samples > 1:
        raise TypeError("Cannot get more than 1 sample if it is not a VAE.")

    trainer = pl.Trainer(**OmegaConf.to_container(cfg.trainer, resolve=True))

    logger.info("Trainer initialized")

    if cfg.jointstype == 'vertices':
        model.transforms.rots2joints.jointstype = "vertices"

    model.eval()
    # x=2
    # d = {'train': {}, 'val': {}}
    # from temos.utils.file_io import write_json
    # for dset_name, dset in zip(['train', 'val'], [train_dataset, val_dataset]):
    #     breakpoint()
    #     for k in dset.keyids:
    #         if k in d[dset_name]:
    #             breakpoint()
    #         d[dset_name][k] = {}
    #         d[dset_name][k]['text'] = dset.texts_data[k]
    #         d[dset_name][k]['len'] = dset._num_frames_in_sequence[k]
    # write_json(d, './data_dict.json')
    path_dur = savepath / 'duration'
    path_div = savepath / 'diversity'
    
    texts = []
    lengths = []
    durs = [1,2]
    with torch.no_grad():
        for text, length in tqdm(zip(texts, lengths)):
            vids = []
            fpath =  '-'.join(text).replace(' ', '_')
            for dur in durs:
                pl.seed_everything(dur.index(durs))

                motion = model.forward_seq(text, length,
                                           align_full_bodies=True,
                                           slerp_window_size=cfg.slerp_ws,
                                           return_type="vertices")
            # np.save(cfg.output, {'motion': motion.numpy(), 'text': texts, 'lengths': lengths} )
                vid_ = visualize_meshes(motion)
                clip = mp.ImageSequenceClip(vid_, fps=30)
                clip = add_text_moviepy(clip, text)
                vids.append(clip)

            vid_path = Path(path_dur / fpath)
            from teach.render.video import stack_vids
            stack_vids_moviepy(vids, savepath=vid_path)
            # save_video_samples(vids[0], vid_path.resolve(), text, fps=30)
        # stack_vids()

            vids = []

            for i in range(cfg.samples):
                pl.seed_everything(i)

                motion = model.forward_seq(text, length,
                                           align_full_bodies=True,
                                           slerp_window_size=cfg.slerp_ws,
                                           return_type="vertices")
            # np.save(cfg.output, {'motion': motion.numpy(), 'text': texts, 'lengths': lengths} )
                vid_ = visualize_meshes(motion)
                clip = mp.ImageSequenceClip(vid_, fps=30)
                clip = add_text_moviepy(clip, text)
                vids.append(clip)

            vid_path = Path(path_div / fpath)
            from teach.render.video import stack_vids
            stack_vids_moviepy(vids, savepath=vid_path)
            
    logger.info(f"The motion is saved there: {savepath}")

    logger.info("Finished analysis for Diversity, Durations of Motions.")
    
if __name__ == "__main__":
    _compare_results()
    # args = parser.parse_args()
    # dirs = args.dirs
    # all_dirs = []
    # import glob
    # d_lst = {}
    # for method in dirs: 
    #     method_name = method.split('/')[1]
    #     if method.split('/')[-1] == 'fast_renders_pairs':
    #         method_name = f'{method_name}_slerp'
    #     d_lst[method_name] = glob.glob(f'{base_dir}/{method}/test/*')
    #     d_lst[method_name].sort(key=lambda x: x.split('/')[-1])
    # from temos.render.video import save_video_samples, stack_vids, put_text

    # # ffmpeg -i inputClip.mp4 -vf f"drawtext=text='{method}':x=200:y=0:fontsize=22:fontcolor=white" -c:a copy {temp_path}.mp4
    # fd = '--VS--'.join(list(d_lst.keys()))
    # from tqdm import tqdm
    # savepath = f'/home/nathanasiou/Desktop/scratch/{fd}'
    # lofs = len(d_lst[list(d_lst.keys())[0]])
    # os.makedirs(savepath, exist_ok=True)

    # for tpl in tqdm(zip(*d_lst.values()), total=lofs):
    #     ltc = [] 
    #     assert len(set([k.split('/')[-1] for k in tpl])) == 1

    #     for i, v in enumerate(tpl):
    #         mtd = v.split('/')[-4]
    #         if 'fast_renders_pairs' in v:
    #             mtd = f'{mtd}_slerp'

    #         vname = put_text(mtd, v, f'{savepath}/temp{i}')

    #         ltc.append(vname)
    #     nm = tpl[0].split('/')[-1]
    #     stack_vids(ltc, f'{savepath}/{nm}', orient='h')
    #     for x in ltc:
    #         os.remove(x)

