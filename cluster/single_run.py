from base import launch_task_on_cluster
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--mode', required=True, choices=['train', 'sample', 
                                                          'eval', 'render', 'fast-render'], type=str,
                            help='Mode is either train or sample or eval!')
    
    parser.add_argument('--folder', required=False, type=str, default=None,
                        help='folder for evaluation')
    parser.add_argument('--expname', required=False, type=str, default='exp_name',
                        help='Experiment Name')
    parser.add_argument('--run-id', required=False, type=str, default='run_id',
                        help='Run ID')
    parser.add_argument('--extras', required=False, default='', type=str, help='args hydra')
    parser.add_argument('--gpus', required=False, default=1, type=int,
                        help='No of GPUS to use')

    parser.add_argument('--bid', required=False, default=10, type=int,
                        help='bid money for cluster')

    arguments = parser.parse_args()
    cluster_mode = arguments.mode
    bid_for_exp = arguments.bid
    gpus_no = arguments.gpus
    if arguments.extras is not None:
        args = arguments.extras
        _args = args.strip().split()
        for i, a in enumerate(_args):
            if '$' in a:
                subst_arg = a.split('=')[-1]
                _args[i] = a.split('=')[0] + '=' + f"'{subst_arg}'"
        args = ' '.join(_args)
    else:
        args = ''
    
    if gpus_no > 1: assert cluster_mode == 'train'

    if cluster_mode == 'train':
        run_id = arguments.run_id
        expname = arguments.expname
        experiments = [{"expname": expname, "run_id": run_id, "args": args, "gpus": gpus_no}]
    elif cluster_mode == 'eval':
        extra_args = arguments.extras
        exp_folder = arguments.folder
        experiments = [{"folder": exp_folder, "args": args, "gpus": gpus_no}]
    elif cluster_mode == 'sample':
        extra_args = arguments.extras
        exp_folder = arguments.folder
        experiments = [{"folder": exp_folder, "args": args, "gpus": gpus_no}]
    elif cluster_mode == 'render':
        extra_args = arguments.extras
        exp_folder = arguments.folder
        experiments = [{"folder": exp_folder, "args": args, "gpus": gpus_no}]
    elif cluster_mode == 'fast-render':
        extra_args = arguments.extras
        exp_folder = arguments.folder
        experiments = [{"folder": exp_folder, "args": args, "gpus": gpus_no}]

    launch_task_on_cluster(experiments, bid_amount=bid_for_exp, 
                           mode=cluster_mode)
