import logging
import hydra
import os
from pathlib import Path
import datetime
import subprocess
import sys
import stat
from loguru import logger
from typing import List, Set, Dict, Tuple, Optional
MODES = ["training", "sample_eval", "render",
         "compile_results", "relaunch_sample"]

SHELL_SCRIPT_FD = 'cluster_scripts'
CONDOR_FD = 'condor_logs'

GPUS = {'v100-p16': ('\"Tesla V100-PCIE-16GB\"', 'tesla', 16000),
        'v100-p32': ('\"Tesla V100-PCIE-32GB\"', 'tesla', 32000),
        'v100-s32': ('\"Tesla V100-SXM2-32GB\"', 'tesla', 32000),
        'a100-sm80': ('\"NVIDIA A100-SXM-80GB\"', 'nvidia', 80000),
        'a100-sxm40': ('\"NVIDIA A100-SXM4-40GB\"', 'nvidia', 40000),
        'quadro6000': ('\"Quadro RTX 6000\"', 'quadro', 24000),
        'rtx2080ti': ('\"NVIDIA GeForce RTX 2080 Ti\"', 'rtx', 11000)
        }
    
SUBMISSION_TEMPLATE = f'executable = RUN_SCRIPT\n' \
                       'arguments = $(Process) $(Cluster)\n' \
                       'error = CNR_LOG_ID/$(Cluster).$(Process).err\n' \
                       'output = CNR_LOG_ID/$(Cluster).$(Process).out\n' \
                       'log = CNR_LOG_ID/$(Cluster).$(Process).log\n' \
                       'request_memory = 64000\n' \
                       'request_cpus=CPUS\n' \
                       'request_gpus=NO_GPUS\n' \
                       'requirements=GPUS\n' \
                       'queue 1'
import shortuuid

def generate_id() -> str:
    # ~3t run ids (36**4)
    run_gen = shortuuid.ShortUUID(alphabet=list("0123456789abcdefghijklmnopqrstuvwxyz"))
    return run_gen.random(4)

ID_TMP = generate_id()
ID_EXP = f'_{ID_TMP}'

def get_gpus(min_mem=32000, arch=('tesla', 'quadro', 'rtx', 'nvidia')):
    gpu_names = []
    for k, (gpu_name, gpu_arch, gpu_mem) in GPUS.items():
        if gpu_mem >= min_mem and gpu_arch in arch:
            gpu_names.append(gpu_name)

    assert len(gpu_names) > 0, 'Suitable GPU model could not be found'

    return gpu_names


def launch_task_on_cluster(configs: List[Dict[str, str]],
                           num_exp: int = 1, mode: str = 'train',
                           bid_amount: int = 10, num_workers: int = 8,
                           memory: int = 64000, gpu_min_mem:int = 20000,
                           gpu_arch: Optional[List[Tuple[str, ...]]] = 
                           ('tesla', 'quadro', 'rtx', 'nvidia')) -> None:

    gpus = get_gpus(min_mem=gpu_min_mem, arch=gpu_arch)
    gpus = ' || '.join([f'CUDADeviceName=={x}' for x in gpus])
    cpus = int(num_workers/2)

    # stamp_submission = "{:%Y_%d_%m_%H:%M:%S}".format(datetime.now())    
    
    # if exp_opts is not None:
    #     bash += ' --opts '
    #     for opt in exp_opts:
    #         bash += f'{opt} '
    #     bash += 'SYSTEM.CLUSTER_NODE $2.$1'
    # else:
    #     bash += ' --opts SYSTEM.CLUSTER_NODE $2.$1'
    
    # assert mode in MODES
    condor_dir = Path(CONDOR_FD)
    shell_dir = Path(SHELL_SCRIPT_FD)
    no_gpus = 1
    if mode == "train":
        for experiment in configs:
            expname = experiment["expname"]
            run_id = experiment["run_id"]
            extra_args = experiment["args"]
            no_gpus = experiment["gpus"]
            sub_file = SUBMISSION_TEMPLATE
            bash = 'export PYTHONBUFFERED=1\nexport PATH=$PATH\n' \
                    f'{sys.executable} train.py ' \
                    f'run_id={run_id} experiment={expname} {extra_args}'
            shell_dir.mkdir(parents=True, exist_ok=True)
            run_cmd_path = shell_dir / (run_id + '_' + mode + ID_EXP +".sh")

            with open(run_cmd_path, 'w') as f:
                f.write(bash)
            os.chmod(run_cmd_path, stat.S_IRWXU)

            log = f'{mode}/{expname}/{run_id}'
            for x, y in [("NO_GPUS", str(no_gpus)), ("GPUS", gpus),
                         ("CNR_LOG_ID", f'{CONDOR_FD}/{log}/logs'),
                         ("CPUS", str(cpus)),
                         ("RUN_SCRIPT", os.fspath(run_cmd_path))]:
                sub_file = sub_file.replace(x, y)

            submission_path = condor_dir / log / (run_id + ID_EXP + ".sub")
            logdir_condor = condor_dir / log / 'logs'
            logdir_condor.mkdir(parents=True, exist_ok=True)

            with open(submission_path, 'w') as f:
                f.write(sub_file)

            logger.info('The cluster logs for this experiments can be found under:'\
                        f'{str(logdir_condor)}')

            cmd = ['condor_submit_bid', f'{bid_amount}', str(submission_path)]
            logger.info('Executing ' + ' '.join(cmd))
            subprocess.run(cmd)
    elif mode in ["sample", "relaunch_sample"]:
        # extract samples 
        # redo it at some point
        for exp in configs:
            sample_script = 'sample_seq'
            folder = exp['folder']
            extra_args = exp["args"]

            sub_file = SUBMISSION_TEMPLATE
            bash = 'export PYTHONBUFFERED=1\nexport PATH=$PATH\n' \
                    f'{sys.executable} {sample_script}.py ' \
                    f'folder={folder} {extra_args}'
            shell_dir.mkdir(parents=True, exist_ok=True)
            import pathlib
            exp_path = pathlib.PurePath(folder)
            expname = exp_path.parent.name
            run_id = exp_path.name
            
            run_cmd_path = shell_dir / (run_id + '_' + mode + ID_EXP + ".sh")

            with open(run_cmd_path, 'w') as f:
                f.write(bash)
            os.chmod(run_cmd_path, stat.S_IRWXU)


            log = f'{mode}/{expname}/{run_id}'
            for x, y in [("NO_GPUS", str(no_gpus)),("GPUS", gpus),
                         ("CNR_LOG_ID", f'{CONDOR_FD}/{log}/logs'),
                         ("CPUS", str(cpus)),
                         ("RUN_SCRIPT", os.fspath(run_cmd_path))]:
                sub_file = sub_file.replace(x, y)

            submission_path = condor_dir / log / (run_id + ID_EXP + ".sub")
            logdir_condor = condor_dir / log / 'logs'
            logdir_condor.mkdir(parents=True, exist_ok=True)

            with open(submission_path, 'w') as f:
                f.write(sub_file)

            logger.info('The cluster logs for this experiments can be found under:'\
                        f'{str(logdir_condor)}')

            cmd = ['condor_submit_bid', f'{bid_amount}', str(submission_path)]
            logger.info('Executing ' + ' '.join(cmd))
            subprocess.run(cmd)


    elif mode == "render":
                  
        for exp in configs:
            sample_script = 'render'
            folder = exp['folder']
            extra_args = exp["args"]
            sub_file = SUBMISSION_TEMPLATE
            bash = 'export PYTHONBUFFERED=1\nexport PATH=$PATH\n' \
                    'export PATH=/home/nathanasiou/apps/imagemagick/bin:$PATH\n' \
                    'export LD_LIBRARY_PATH=/home/nathanasiou/apps/imagemagick/lib:$LD_LIBRARY_PATH\n' \
                    f'export HOME=/home/nathanasiou \nBLENDERPATH=$HOME/apps/blender/blender-3.1.2-linux-x64\n' \
                    f'export PYTHONPATH=$PYTHONPATH:$HOME/.venvs/for_blender/lib/python3.10/site-packages\n' \
                    f'$BLENDERPATH/blender --background --python render.py -- npy={folder} {extra_args}'
            shell_dir.mkdir(parents=True, exist_ok=True)
            import pathlib
            exp_path = pathlib.PurePath(folder)
            expname = exp_path.parent.name
            run_id = exp_path.name

            run_cmd_path = shell_dir / (run_id + '_' + mode + ID_EXP + ".sh")

            with open(run_cmd_path, 'w') as f:
                f.write(bash)
            os.chmod(run_cmd_path, stat.S_IRWXU)
            cpus = 16
            log = f'{mode}/{expname}/{run_id}'
            for x, y in [("GPUS", gpus),("NO_GPUS", str(no_gpus)),
                         ("CNR_LOG_ID", f'{CONDOR_FD}/{log}/logs'),
                         ("CPUS", str(cpus)),
                         ("RUN_SCRIPT", os.fspath(run_cmd_path))]:
                sub_file = sub_file.replace(x, y)

            submission_path = condor_dir / log / (run_id + ID_EXP + ".sub")
            logdir_condor = condor_dir / log / 'logs'
            logdir_condor.mkdir(parents=True, exist_ok=True)

            with open(submission_path, 'w') as f:
                f.write(sub_file)

            logger.info('The cluster logs for this experiments can be found under:'\
                         f'\n{str(logdir_condor)}')

            cmd = ['condor_submit_bid', f'{bid_amount}', str(submission_path)]
            logger.info('Executing\n' + ' '.join(cmd))
            subprocess.run(cmd)

    elif mode == "eval":
        for exp in configs:
            eval_script = 'eval'
            folder = exp['folder']
            extra_args = exp["args"]

            sub_file = SUBMISSION_TEMPLATE
            bash = 'export PYTHONBUFFERED=1\nexport PATH=$PATH\n' \
                    f'{sys.executable} {eval_script}.py ' \
                    f'folder={folder} {extra_args}'
            shell_dir.mkdir(parents=True, exist_ok=True)
            import pathlib
            exp_path = pathlib.PurePath(folder)
            expname = exp_path.parent.name
            run_id = exp_path.name

            run_cmd_path = shell_dir / (run_id + f'_{eval_script}'+ ID_EXP + '.sh')

            with open(run_cmd_path, 'w') as f:
                f.write(bash)
            os.chmod(run_cmd_path, stat.S_IRWXU)

            log = f'{mode}/{expname}/{run_id}'
            for x, y in [("GPUS", gpus),("NO_GPUS", str(no_gpus)),
                         ("CNR_LOG_ID", f'{CONDOR_FD}/{log}/logs'),
                         ("CPUS", str(cpus)),
                         ("RUN_SCRIPT", os.fspath(run_cmd_path))]:
                sub_file = sub_file.replace(x, y)

            submission_path = condor_dir / log / (run_id + ID_EXP + ".sub")
            logdir_condor = condor_dir / log / 'logs'
            logdir_condor.mkdir(parents=True, exist_ok=True)

            with open(submission_path, 'w') as f:
                f.write(sub_file)

            logger.info('The cluster logs for this experiments can be found under:'\
                        f'{str(logdir_condor)}')

            cmd = ['condor_submit_bid', f'{bid_amount}', str(submission_path)]
            logger.info('Executing ' + ' '.join(cmd))
            subprocess.run(cmd)

    elif mode == "fast-render":
        for exp in configs:
            script = 'render_video_fast'
            folder = exp['folder']
            extra_args = exp["args"]

            sub_file = SUBMISSION_TEMPLATE
            bash = 'export PYTHONBUFFERED=1\nexport PATH=$PATH\n' \
                    f'{sys.executable} {script}.py ' \
                    f'folder={folder} {extra_args}'
            shell_dir.mkdir(parents=True, exist_ok=True)
            import pathlib
            exp_path = pathlib.PurePath(folder)
            expname = exp_path.parent.name
            run_id = exp_path.name
            run_cmd_path = shell_dir / (run_id + f'_{script}'+ ID_EXP + '.sh')

            with open(run_cmd_path, 'w') as f:
                f.write(bash)
            os.chmod(run_cmd_path, stat.S_IRWXU)

            log = f'{mode}/{expname}/{run_id}'
            for x, y in [("GPUS", gpus),("NO_GPUS", str(no_gpus)),
                         ("CNR_LOG_ID", f'{CONDOR_FD}/{log}/logs'),
                         ("CPUS", str(cpus)),
                         ("RUN_SCRIPT", os.fspath(run_cmd_path))]:
                sub_file = sub_file.replace(x, y)

            submission_path = condor_dir / log / (run_id + ".sub")
            logdir_condor = condor_dir / log / 'logs'
            logdir_condor.mkdir(parents=True, exist_ok=True)

            with open(submission_path, 'w') as f:
                f.write(sub_file)

            logger.info('The cluster logs for this experiments can be found under:'\
                        f'{str(logdir_condor)}')

            cmd = ['condor_submit_bid', f'{bid_amount}', str(submission_path)]
            logger.info('Executing ' + ' '.join(cmd))
            subprocess.run(cmd)
