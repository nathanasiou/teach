from base import launch_task_on_cluster

expname = "check-kit-babel"
# using direcotry like eg path/to expname causes this bug:
# watch out for now weird to get it resolved it does not if I don't use /
# temos/logger/wandb_log.py", line 37, in symlink_checkpoint
# os.symlink(Path(code_dir) / "wandb" / project / run_id,
# FileExistsError: [Errno 17] File exists: 
# '/home/nathanasiou/Desktop/conditional_action_gen/teach/wandb/teach/amass-xyz' -> 'wandb/teach/amass-xyz'

experiments_amass = [
    {
        "expname": expname,
        "run_id": "kit-mmm-pos",
        "args": 'data=kit-mmm-xyz',
    }]


if __name__ == "__main__":
    launch_task_on_cluster(experiments_amass, mode='train')
