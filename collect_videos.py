from pathlib import Path

from cv2 import exp
from teach.utils.file_io import write_json
from teach.render.video import stack_vids, stack_vids_moviepy
import os
import uuid
import sys
import IPython
# sys.breakpointhook = IPython.embed
from teach.render.video import stack_vids, put_text, add_text_moviepy, Video
from tqdm import tqdm
base_path = Path('/is/cluster/work/nathanasiou/experiments/teach/babel-amass/teach-post-submission/')
experiments = ['hist-15-bs-32-lr-1e-5', 'hist-15-bs-32-z_prev-w-hist', 'hist-15-bs-32',
 'hist-15-bs-32-lr-5e-4', 'hist-15-bs-32-z_prev-only']

# Z analysis
# exp_to_merge = ['hist-15-bs-32', 'hist-15-bs-32-z_prev-only','hist-15-bs-32-z_prev-w-hist']

# LR analysis
exp_to_merge = ['hist-15-bs-32-lr-5e-4', 'hist-15-bs-32','hist-15-bs-32-lr-1e-5']

exp_to_merge = list(base_path.glob('temos-w_motion_branch-*sec-all_actions'))
# experiments = ['hist-15-bs-32', 'hist-10-bs-32']
if exp_to_merge[0].is_dir():
    exp_to_merge = [str(x).split('/')[-1] for x in exp_to_merge]
name = 'walk-analysis-all-actions'
outdir = f'/home/nathanasiou/Desktop/scratch/teach/post_submission/{name}'


# sample_variants = ['train', 'val']
sample_variants = ['walk_train', 'walk_val']

vid_d = {}
keys_in_common = []
keep_key = {}

for var, sample_variant in enumerate(sample_variants):
    vid_d[sample_variant] = {}
    
    if sample_variant in ['train', 'val']:
        vid_d[sample_variant]['reference'] = []
        keep_key[sample_variant] = []
    keys_in_common = []
    for i, expname in enumerate(exp_to_merge):
        path = Path(base_path / expname / 'videos' / sample_variant / 'bodies')
        vid_d[sample_variant][expname] = []
        if not path.is_dir():
            print(f"Buggy path at {str(path)}")
            continue
        
        for vid_p in list(path.iterdir()):
            if '_gt' in str(vid_p):
                
                if '_vs_gt' in str(vid_p):
                    continue

                elif i == 0 and sample_variant in ['train', 'val']:
                    # exp_out = Path(f'{outdir} / reference')
                    # exp_out.mkdir(exist_ok=True,  parents=True)
                    # new_flp = Path(exp_out/(str(vid_p.name) + '.mp4'))
                    if sample_variant in ['train', 'val']:
                        keys_in_common.append(vid_p.stem.split('_')[0])
                    vid_d[sample_variant]['reference'].append(vid_p)
                    # add_text_moviepy(expname, str(vid_p), new_flp)
                    vid_d[sample_variant]['reference'].sort()
            else:                    
                # exp_out = Path(f'{outdir} / {expname}')
                # exp_out.mkdir(exist_ok=True,  parents=True)
                # new_flp = Path(exp_out/(str(vid_p.name) + '.mp4'))
                if sample_variant in ['train', 'val']:
                    keys_in_common.append(vid_p.stem)
                
                vid_d[sample_variant][expname].append(vid_p)
                # add_text_moviepy(expname, str(vid_p), new_flp)
        vid_d[sample_variant][expname].sort()

    if sample_variant in ['train', 'val']:
        from collections import Counter
        cc = Counter(keys_in_common)
        for el, oc in cc.items():
            if oc == (len(exp_to_merge) + 1): # +1 for groundtruth
                keep_key[sample_variant].append(el)
            # keys_for_exp.append(el)
# for k, v in vid_d.items():
#     dict_of_exp = v
#     for k2, v2 in dict_of_exp.items():
#         print(len(v2))
outdir = Path(f'{outdir}/')
outdir.mkdir(exist_ok=True, parents=True)


lists_to_cat = []

for variant, exp_list in vid_d.items():
    # file_iter = [dict(zip(exp_list.keys(), v)) for v in zip(*exp_list.values())]
    p2save = Path(outdir/variant)
    p2save.mkdir(exist_ok=True, parents=True)

    a_key = next(iter(exp_list))
    num_of_walks = len(exp_list[a_key])
    for i in tqdm(range(num_of_walks)):
        list_to_cat = []
        for exp_name, paths in exp_list.items():
            if len(paths) == 0: continue
            cur_vid = Video(str(paths[i]))# add_text_moviepy(str(video_p), exp_name)
            cur_vid.add_text(exp_name)
            temp_path = f'{p2save}/{str(uuid.uuid4())}.mp4'
            cur_vid.save(temp_path)
            list_to_cat.append(temp_path)
        if list_to_cat:
            fname = paths[i].stem.replace('_', '')
            stack_vids_moviepy(list_to_cat, f'{p2save}/{fname}_stack.mp4')
            for temp_vid in list_to_cat:
                os.remove(temp_vid)
    

exit()
for variant, exp_list in vid_d.items():
    file_iter = [dict(zip(exp_list.keys(), v)) for v in zip(*exp_list.values())]
    
    p2save = Path(outdir/variant)
    p2save.mkdir(exist_ok=True, parents=True)
    
    for exp_files in tqdm(file_iter):
        list_to_cat = []
        for exp_name, video_p in exp_files.items(): 
            if variant in ['train', 'val']: # for walk we don't need it
                if video_p.stem in keep_key[variant] or (exp_name == 'reference' and video_p.stem.split('_')[0] in keep_key[variant]):
                    cur_vid = Video(str(video_p))# add_text_moviepy(str(video_p), exp_name)
                    cur_vid.add_text(exp_name)
                    temp_path = f'{p2save}/{str(uuid.uuid4())}.mp4'
                    cur_vid.save(temp_path)
                    list_to_cat.append(temp_path)

            else:
                cur_vid = Video(str(video_p))# add_text_moviepy(str(video_p), exp_name)
                cur_vid.add_text(exp_name)
                temp_path = f'{p2save}/{str(uuid.uuid4())}.mp4'
                cur_vid.save(temp_path)
                list_to_cat.append(temp_path)
        if list_to_cat:
            if  'walk' in str(video_p):
                fname = video_p.stem
            else:
                fname = video_p.stem.split('_')[0]
            stack_vids_moviepy(list_to_cat, f'{p2save}/{fname}_stack.mp4')
            for temp_vid in list_to_cat:
                os.remove(temp_vid)
    # result = [dict(zip(exp_list.keys(), v)) for v in zip(*exp_list.values())]
    
