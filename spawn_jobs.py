import subprocess
from pathlib import Path
import sys
from loguru import logger
import glob


v=True 
def end_script():
    from inspect import currentframe, getframeinfo
    frameinfo = getframeinfo(currentframe())
    sys.exit(f'Exited after line {frameinfo.filename} {frameinfo.lineno}')
    
def run(cmd):
    logger.info(f"Executing: {' '.join(cmd)}")
    x = subprocess.call(cmd)

# teach/independent sampling
base_dir = Path("/is/cluster/work/nathanasiou/experiments/teach/babel-amass/teach-post-submission")
# temos/teach final exps

# exps_to_sample = ['teach-motion_branch_false-teacher_force_false',
#                   'teach-motion_branch_false-teacher_force_true',
#                   'teach-motion_branch_true-teacher_force_false',
#                   'teach-motion_branch_true-teacher_force_true',
#                   'temos-motion_branch_false-teacher_force_false',
#                   'temos-motion_branch_true-teacher_force_false'
# models = ['teach', 'temos']
# motion_branch = ['true', 'false']
# teacher_forcing = ['true', 'false']
# hist_frames= [1, 2, 5, 10]
# for hf in hist_frames:
#     for is_motion_branch in motion_branch:
#         for tf in teacher_forcing:
#             if hf == 5 or hf == 10 and is_motion_branch == 'false':
#                 continue
#             else:
#                 cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']
#                 cmd.extend(['--run-id', f'teach-hf-{hf}-motion_branch_{is_motion_branch}-teacher_force_{tf}', '--extras',
#                         f"model.teacher_forcing={tf} model.hist_frames={hf} model.motion_branch={is_motion_branch} data.dtype=separate_pairs callback.render.every_n_epochs=50 model=teach"])
#                 run(cmd)
# TEMOS SAMPLING
#base_dir = Path("/is/cluster/work/nathanasiou/experiments/teach-post-submission-29Aug/")

#exps_to_sample = ['temos-motion_branch_true-teacher_force_false',
#                  'temos-motion_branch_false-teacher_force_false']
# slerp = ['null', 8]
# align = ['full', 'trans']
#combinations = [['null', 'trans'], [8, 'full']]
#for exp in exps_to_sample:
#    exp_path = Path(f'{base_dir}/{exp}/')
#    for c in combinations:
#        slerp = c[0]
#        align = c[1]
#        cmd = ['python', 'cluster/single_run.py', '--folder', str(exp_path), '--mode', 'sample', '--bid', '20']
#        cmd.extend(['--extras', f'align={align} slerp_ws={slerp}'])
#        run(cmd)

#end_script()

base_dir = Path("/is/cluster/work/nathanasiou/experiments/teach-post-submission-29Aug/")

exps_to_sample = ['teach-hf-1-motion_branch_true-teacher_force_false',
                  'teach-hf-1-motion_branch_true-teacher_force_true',
                  'teach-hf-2-motion_branch_true-teacher_force_false',
                  'teach-hf-2-motion_branch_true-teacher_force_true',
                  'teach-hf-5-motion_branch_true-teacher_force_false',
                  'teach-hf-5-motion_branch_true-teacher_force_true',
                  'teach-hf-10-motion_branch_true-teacher_force_false',
                  'teach-hf-10-motion_branch_true-teacher_force_true']

# pairs_type = ['samples_no-slerp_unaligned_pairs',
#               'samples_slerp_aligned_pairs',
#               'samples_slerp_unaligned_pairs',
#               'samples_no-slerp_aligned_pairs']

#set_folder = 'checkpoint-last/val'
#slerp = ['true', 'false']
combs = [['true', 'true'], ['false', 'false']]
for exp in exps_to_sample:
    for c in combs:
        slerp = c[0]
        align = c[1]
        samples_path = Path(f'{base_dir}/{exp}/')
        cmd = ['python', 'cluster/single_run.py', '--folder', str(samples_path), '--mode', 'eval', '--bid', '40']
        cmd.extend(['--extras', f'align={align} slerp={slerp}'])
        run(cmd)

end_script()

# slerp = ['null', 8]
# align = ['full', 'trans']
combinations = [['null', 'trans'], [8, 'full']]
for exp in exps_to_sample:
    exp_path = Path(f'{base_dir}/{exp}/')
    for c in combinations:
        slerp = c[0]
        align = c[1]
        cmd = ['python', 'cluster/single_run.py', '--folder', str(exp_path), '--mode', 'sample', '--bid', '20']
        cmd.extend(['--extras', f'align={align} slerp_ws={slerp}'])
        run(cmd)=
# python cluster/single_run.py --folder /is/cluster/work/nathanasiou/experiments/teach-post-submission-29Aug/teach-hf-1-motion_branch_true-teacher_force_true --mode sample --bid 20 --extras align=true slerp_ws=true

# submission only samples

#for exp in ['teach-motion_branch_false-teacher_force_false', 'teach-motion_branch_false-teacher_force_true',
#            'teach-motion_branch_true-teacher_force_false', 'teach-motion_branch_true-teacher_force_true',
#            'temos-motion_branch_true-teacher_force_false']:
#     exp_path = Path(f'{base_dir}/{exp}/')
#     cmd = ['python', 'cluster/single_run.py', '--folder', str(exp_path), '--mode', 'sample', '--bid', '20']
#     cmd.extend([ '--extras',
#                 f"jointstype=vertices align=full slerp_ws=8 set=submission"])
#     run(cmd)

#for exp in ['teach-motion_branch_false-teacher_force_false', 'teach-motion_branch_false-teacher_force_true',
#            'teach-motion_branch_true-teacher_force_false', 'teach-motion_branch_true-teacher_force_true',
#            'temos-motion_branch_true-teacher_force_false']:
#     exp_path = Path(f'{base_dir}/{exp}/')
#     cmd = ['python', 'cluster/single_run.py', '--folder', str(exp_path), '--mode', 'sample', '--bid', '20']
#     cmd.extend([ '--extras',
#                 f"jointstype=vertices align=trans slerp_ws=null set=submission"])
#     run(cmd)
#end_script()
#end_script()
# # ablation history frames samples
# exp_ablate_h = ['teach-hist10-teacher_force',
#                 'teach-hist10-teacher_force_false',
#                 'teach-hist5-teacher_force',
#                 'teach-hist5-teacher_force_false']
# for exp in exp_ablate_h:
#     exp_path = Path(f'{base_dir}/{exp}/')
#     cmd = ['python', 'cluster/single_run.py', '--folder', str(exp_path), '--mode', 'sample', '--bid', '20']
#     cmd.extend([ '--extras',
#                 f"align=full slerp_ws=8"])
#     run(cmd)

# teach/independent evaluation

# base_dir = Path("/is/cluster/work/nathanasiou/experiments/teach/babel-amass/teach-post-submission")
exps_to_sample = ['teach-motion_branch_false-teacher_force_false',
                  'teach-motion_branch_false-teacher_force_true',
                  'teach-motion_branch_true-teacher_force_false',
                  'teach-motion_branch_true-teacher_force_true',
                  'temos-motion_branch_false-teacher_force_false',
                  'temos-motion_branch_true-teacher_force_false',
                  'teach-hist10-teacher_force',
                  'teach-hist10-teacher_force_false',
                  'teach-hist5-teacher_force',
                  'teach-hist5-teacher_force_false']

# pairs_type = ['samples_no-slerp_unaligned_pairs',
#               'samples_slerp_aligned_pairs',
#               'samples_slerp_unaligned_pairs',
#               'samples_no-slerp_aligned_pairs']

#set_folder = 'checkpoint-last/val'
#slerp = ['true', 'false']
#align = ['true', 'false']
#for exp in exps_to_sample:
#    for slerp in ['true', 'false']:
#        for align in ['true', 'false']:
#            samples_path = Path(f'{base_dir}/{exp}/')
#            cmd = ['python', 'cluster/single_run.py', '--folder', str(samples_path), '--mode', 'eval', '--bid', '40']
#            cmd.extend(['--extras', f'align={align} slerp={slerp}'])
#            run(cmd)

# submission only samples
# for exp in ['teach-motion_branch_false-teacher_force_false', 'teach-motion_branch_false-teacher_force_true',
#             'teach-motion_branch_true-teacher_force_false', 'teach-motion_branch_true-teacher_force_true']:
#     samples_path = Path(f'{base_dir}/{exp}/samples_slerp_aligned_pairs/checkpoint-last/submission')
#     for fig_var in ['true', 'false']:
#         cmd = ['python', 'cluster/single_run.py', '--folder', str(samples_path), '--mode', 'render', '--bid', '20']
#         cmd.extend([ '--extras',
#                     f"mode=sequence quality=false num=5 res=med separate_actions={fig_var} fake_trans=false"])
#         run(cmd)
end_script()
# # single example rendering
for exp in ['teach-motion_branch_true-teacher_force_false',
            'temos-motion_branch_true-teacher_force_false']:
    samples_path = Path(f'{base_dir}/{exp}/samples_vertices_slerp_aligned_pairs/checkpoint-last/submission')
    cmd = ['python', 'cluster/single_run.py', '--folder', str(samples_path), '--mode', 'render', '--bid', '20']
    cmd.extend([ '--extras',
                f"mode=video quality=false num=5 res=med fake_trans=false"])
    run(cmd)

# wo slerp
for exp in ['teach-motion_branch_true-teacher_force_false',
            'temos-motion_branch_true-teacher_force_false']:
    samples_path = Path(f'{base_dir}/{exp}/samples_vertices_no-slerp_unaligned_pairs/checkpoint-last/submission')
    cmd = ['python', 'cluster/single_run.py', '--folder', str(samples_path), '--mode', 'render', '--bid', '20']
    cmd.extend([ '--extras',
                f"mode=video quality=false num=5 res=med fake_trans=false"])
    run(cmd)


# # single example rendering
#for exp in ['teach-motion_branch_true-teacher_force_false',
#            'temos-motion_branch_true-teacher_force_false']:
#    samples_path = Path(f'{base_dir}/{exp}/samples_vertices_slerp_aligned_pairs/checkpoint-last/submission/1183-3.npy')
#    for fig_var in ['true', 'false']:
#
#        cmd = ['python', 'cluster/single_run.py', '--folder', str(samples_path), '--mode', 'render', '--bid', '20']
#        cmd.extend([ '--extras',
#                    f"mode=sequence quality=false num=5 res=med separate_actions={fig_var} fake_trans=true"])
#        run(cmd)
#        cmd = ['python', 'cluster/single_run.py', '--folder', str(samples_path), '--mode', 'render', '--bid', '20']
#        cmd.extend([ '--extras',
#                    f"mode=sequence quality=false num=5 res=med separate_actions={fig_var} fake_trans=false"])
#        run(cmd)
#end_script()
# temos/teach final exps
# models = ['teach', 'temos']
# motion_branch = ['true', 'false']
# teacher_forcing = ['true', 'false']
# for model in models:
#     for is_motion_branch in motion_branch:
#         if model == 'teach':
#             for tf in teacher_forcing:
#                 cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']
#                 cmd.extend(['--run-id', f'{model}-motion_branch_{is_motion_branch}-teacher_force_{tf}', '--extras',
#                         f"model.teacher_forcing={tf} model.motion_branch={is_motion_branch} data.dtype=separate_pairs callback.render.every_n_epochs=50 model={model}"])
#                 run(cmd)

#         elif model == 'temos':
#             cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']
#             cmd.extend(['--run-id', f'{model}-motion_branch_{is_motion_branch}-teacher_force_{tf}', '--extras',
#                         f"model.motion_branch={is_motion_branch} data.dtype='' callback.render.every_n_epochs=50 model={model}"])            
#             run(cmd)


# for p in search_dir.glob('temos-w_motion_branch-*sec-all-actions'):
#     for walk in ['false', 'true']:
#         cmd = ['python', 'cluster/single_run.py', '--folder', str(p)]
#         cmd.extend(['--mode','fast-render', '--bid', '20', '--extras', f'slerp=false mode=extract walk={walk}'])
#         if v:
#             print('Executing:', ' '.join(cmd))
#         x = subprocess.call(cmd)

end_script()
# # Fast render experiments
# for p in search_dir.glob('temos-w_motion_branch-*sec-walk_only'):
#     for walk in ['false', 'true']:
#         cmd = ['python', 'cluster/single_run.py', '--folder', str(p)]
#         cmd.extend(['--mode','fast-render', '--bid', '20', '--extras', f'slerp=false mode=extract walk={walk}'])
#         if v:
#             print('Executing:', ' '.join(cmd))
#         x = subprocess.call(cmd)

# WALK ONLY with MOTION BRANCH, fixed durations + MAX/MIN FRAMES
#for dur in [30,60,90,120,150,180]:
#    cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']
#    cmd.extend(['--run-id', f'temos-w_motion_branch-maxmin-{dur//30}sec-walk_only', '--extras',
#                f"sampler.min_len=45 sampler.max_len=150 model.motion_branch=true data.dtype='' data.walk_only=true callback.render.every_n_epochs=50 model=temos  machine.batch_size=32 sampler.request_frames={dur}"])
#    if v:
#        print('Executing:', ' '.join(cmd))
#    x = subprocess.call(cmd)

# for dur in [30,60,90,120,150,180]:
#     cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']
#     cmd.extend(['--run-id', f'temos-{dur//30}sec-walk_only', '--extras',
#                 f"sampler.min_len=45 sampler.max_len=150 model.motion_branch=true data.dtype='' data.walk_only=true callback.render.every_n_epochs=50 model=temos  machine.batch_size=32 sampler.request_frames={dur}"])
#     if v:
#         print('Executing:', ' '.join(cmd))
#     x = subprocess.call(cmd)

# TEACH + LAMBDA RECONSTRUCTION ABLATION
#for lamda in [2.0, 3.0, 5.0, 7.0]:
#    cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']

#    cmd.extend(['--run-id', f'teach_ld_recons-{lamda}-10fr', '--extras',
#                f"model.losses.lmd_rfeats_recons={lamda} data.dtype=separate_pairs model.hist_frames=15 callback.render.every_n_epochs=50 model=teach machine.batch_size=32"])
#    if v:
#        print('Executing:', ' '.join(cmd))
#    x = subprocess.call(cmd)

#end_script()

for dur in [30,60,90,120,150,180]:
    cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']
    cmd.extend(['--run-id', f'temos-w_motion_branch-{dur//30}sec-all-actions', '--extras',
                f"model.motion_branch=true data.dtype='' callback.render.every_n_epochs=50 model=temos  machine.batch_size=32 sampler.request_frames={dur}"])
    if v:
        print('Executing:', ' '.join(cmd))
    x = subprocess.call(cmd)

#  KL ablation
# for lamda in [1e-1, 1e-3]:
#     cmd = ['python', 'cluster/single_run.py', '--expname', 'teach-post-submission', '--mode', 'train', '--bid', '20']

#     cmd.extend(['--run-id', f'teach_kl-{lamda}-10fr', '--extras', f"model.losses.lmd_kl={lamda} data.dtype=separate_pairs model.hist_frames=10 callback.render.every_n_epochs=50 model=teach machine.batch_size=32"])
